package nmps.fileserver;

/**
 * A THREAD CLASS FOR FILE SERVER
 * 
 */

import java.io.IOException;
import java.net.*;

import nmps.fileserver.state.login.LoginState;
import nmps.fileserver.util.Logger;


public class ServerThread extends Thread
{
	private Socket	socket;

	public ServerThread(Socket socket)
	{
		this.socket = socket;
	}

	public void run()
	{
		Test.doTest(socket);
//		Context context = new Context(new LoginState()); //注册第一个登陆状态
//		context.socket = socket;
//		context.handle();
//
//		/*线程结束，关闭SOCKET*/
		try {
			socket.close();
			Logger.debug("线程关闭");
		} catch (IOException e) {
			Logger.error("线程关闭异常", e);
		}
	}
}
