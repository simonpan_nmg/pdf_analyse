package nmps.fileserver;

/**
 * 状态接口
 * 
 * @author simonpan
 * */
public interface IState
{
	public void handle(Context context);
}
