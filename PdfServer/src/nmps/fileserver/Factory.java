package nmps.fileserver;

/**
 * 获取状态工厂类
 * 
 * <p>利用反射机制获取相应状态</p>
 * 
 * @author simonpan
 *
 */
public class Factory
{
	public static IState getState(String className) throws ClassNotFoundException, InstantiationException, IllegalAccessException
	{
		if(null == className) return null;
		IState cmd = null;
		cmd = (IState)Class.forName(className).newInstance();
		return cmd;
	}
}

