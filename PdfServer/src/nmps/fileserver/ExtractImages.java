/**
 * TO-DO this is a tips <identify -format " %x|%wx%h|%z|%r" test.jpg>
 * http://www.imagemagick.org/script/escape.php
 * */

package nmps.fileserver;

import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;

import nmps.fileserver.config.Config;
import nmps.fileserver.util.ImageConvert;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDResources;
import org.apache.pdfbox.pdmodel.encryption.AccessPermission;
import org.apache.pdfbox.pdmodel.encryption.StandardDecryptionMaterial;
import org.apache.pdfbox.pdmodel.graphics.color.PDColorSpace;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDXObjectImage;

/**
 * This will read a read pdf and extract images. <br/>
 * <br/>
 * 
 * 
 * @author <a href="mailto:simonpan@nmg.com.hk">SimonPan</a>
 * @version $Revision: 1.0 $
 */
public class ExtractImages {
	private int imageCounter = 1;

	ExtractImages() {
	}

	/**
	 * This is the entry point for the application.
	 * 
	 * @param args
	 *            The command-line arguments.
	 * 
	 * @throws Exception
	 *             If there is an error decrypting the document.
	 */
	public static void main(String[] args) throws Exception {
	}

	public String extractImages(String pdfFile) throws Exception {
		String password = "";
		String prefix = null;

		prefix = pdfFile.substring(0, pdfFile.length() - 4);
		PDDocument document = null;
		String json = "";

		try {
			document = PDDocument.load(pdfFile);

			if (document.isEncrypted()) {

				StandardDecryptionMaterial spm = new StandardDecryptionMaterial(
						password);
				document.openProtection(spm);
				AccessPermission ap = document.getCurrentAccessPermission();

				if (!ap.canExtractContent()) {
					throw new IOException(
							"Error: You do not have permission to extract images.");
				}
			}

			List pages = document.getDocumentCatalog().getAllPages();
			Iterator iter = pages.iterator();
			int pageCounter = 1;
			while (iter.hasNext()) {
				PDPage page = (PDPage) iter.next();
				PDResources resources = page.getResources();
				Map images = resources.getImages();
				if (images != null) {
					Iterator imageIter = images.keySet().iterator();
					while (imageIter.hasNext()) {
						if (imageCounter == 501)
							break;

						String key = (String) imageIter.next();
						PDXObjectImage image = (PDXObjectImage) images.get(key);
						String name = getUniqueFileName(key, image.getSuffix());
//						System.out.println("Writing image:" + name);
						// PDStream pdfstream=image.getPDStream();
						String imageFile = new File(pdfFile).getPath() + name;

						String extractFile = new File(imageFile + '.'
								+ image.getSuffix()).getName();

						image.write2file(imageFile);
						String dpi = ImageConvert.executeReturn("identify -format %x "+imageFile+".jpg");
						new File(imageFile+".jpg").delete();
						
						json += extractFile + "," 
								+ image.getBitsPerComponent()
								+ "," + getColorSpace(image.getColorSpace())
								+ "," + dpi.split("\\D+")[0]
								+ "," + image.getWidth() + ","
								+ image.getHeight() + ","
								+ (pageCounter);

						json += "|";
						
//						new File(extractFile).delete();
						
						BufferedImage myImg = image.getRGBImage();

						FileOutputStream out = new FileOutputStream(imageFile
								+ "."+image.getSuffix());
						// File file = new File("newFile.jpg");

						try {
							ImageIO.write((RenderedImage) myImg, "png", out);
						} catch (Exception e) {
							// e.printStackTrace();
							continue;
						}

						// 转图
						String convStr = "convert "
								+ imageFile
								+ '.'
								+ image.getSuffix()
								+ "[0] -flatten -strip -resize 97x122  -quality "
								+ Config.PICTURE_QUALITY
								+ "  -colorspace rgb  " + imageFile + '.'
								+ image.getSuffix();
//						System.out.println("这里是" + convStr);
						ImageConvert.execute(convStr);

					}
				}
				pageCounter++;
			}
		} finally {
			if (document != null) {
				document.close();
			}
		}
		return json;
	}

	private String getColorSpace(PDColorSpace colorspace) {
		String cs = colorspace.getName();
		// if(cs.toUpperCase().contains("CMYK")) return "CMYK";
		// if(cs.toUpperCase().contains("RGB")) return "RGB";
		return cs;
	}

	private String getUniqueFileName(String prefix, String suffix) {
		String uniqueName = null;
		File f = null;
		while (f == null || f.exists()) {
			uniqueName = "p-" + imageCounter;
			f = new File(uniqueName + "." + suffix);
			imageCounter++;
		}
		return uniqueName;
	}
}
