package nmps.fileserver;

import java.io.*;
import java.net.*;

import nmps.fileserver.config.Config;
import nmps.fileserver.util.Logger;

/**
 * 文件服务器主进程
 * 
 * @author simonpan
 *
 */
public class Server
{
	protected int	listenPort	= Config.LISTEN_PORT;

	public static void main(String[] args)
	{
		Server server = new Server();
		server.acceptConnections();
	}


	/**
	 * 接受SOCKET连接
	 */
	public void acceptConnections()
	{
		try {
			ServerSocket server = new ServerSocket(listenPort);
			Socket socket = null;
			Logger.debug("FILE SERVER START!");
			while (true) {
				socket = server.accept();
				// socket.setSoTimeout(50000);
				new ServerThread(socket).start();
			}
		} catch (BindException e) {
			System.out.println("Unable to bind to port " + listenPort);
		} catch (IOException e) {
			System.out.println("Unable to instantiate a ServerSocket on port: "
					+ listenPort);
		}
	}
}
