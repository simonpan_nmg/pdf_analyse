package nmps.fileserver;

public class Lock
{
	private static final Lock instance = new Lock();
	
	private Lock(){}
	
	private String lockFlag;
	
	public static final Lock getInstance()
	{
		return instance;
	}
	
	public void setLockFlag(String lockFlag)
	{
		this.lockFlag = lockFlag;
	}
	
	public String getLockFlag()
	{
		return this.lockFlag;
	}
}