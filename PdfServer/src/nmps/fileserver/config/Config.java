package nmps.fileserver.config;
/*THIS IS A CONFIG FILE FOR THE FILE SERVER*/

public final class Config {
    public static final String ROOT_PATH = "/home/web/nmps/Files/file/lun1/"; // WHERE TO SAVA THE FILE

 	//public static String ROOT_PATH = "d:\\Files\\";
    
	public static final String FILE_PATH = ROOT_PATH + "upload/";
	
	public static final String THUMB_PATH = ROOT_PATH + "thumb/";

	public static final String LOW_PATH = ROOT_PATH + "low/";
	   
    public static final String PSW = "A23%^SDF!!OK"; // PRIVILEGE TO ACCESS THIS SERVER
    
    public static final String LOG_PATH = "./logs/access.log"; // STORE LOG FILES
    
    public static final int LISTEN_PORT = 1024; // SOCKET PORT
    
    public static final int BUFFER_SIZE = 6553600; // BUFFER SIZE
    
    public static final String STOP_SIGNAL = "<stop>"; // STOP SIGNAL
    
    public static final String PICTURE_QUALITY = "50%";
    
    public static final String HTTP_SERVER = "T";
    
    public static final String[] VALID_FILE = {".jpg",".jpeg",".gif"};
    
    public static final String FILE_NOT_EXIST = ROOT_PATH + "error.gif";
    
    public static final boolean DEBUG_MODE = false;
}
