package nmps.fileserver.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.Socket;

import javax.xml.parsers.ParserConfigurationException;

import nmps.fileserver.config.Config;

public class DataTransportor
{
	public Socket	socket;

	public DataTransportor()
	{
	}

	public DataTransportor(Socket socket)
	{
		this.socket = socket;
	}

	/**
	 * 请求数据
	 * 
	 * @return
	 * @throws IOException
	 */
	public String request() throws IOException
	{
		DataInputStream inputStream = new DataInputStream(socket
				.getInputStream());
		return inputStream.readUTF();
	}

	/**
	 * 请求数据指令
	 * 
	 * @return
	 * @throws IOException
	 * @throws ParserConfigurationException
	 */
	public String requestCommand() throws IOException
	{
		DataInputStream inputStream = new DataInputStream(socket
				.getInputStream());
		String xml = inputStream.readUTF();
		return XMLParser.parseXML(xml, "command");
	}

	/**
	 * 返回数据
	 * 
	 * @param cmd
	 * @throws IOException
	 */
	public void response(String cmd) throws IOException
	{
		DataOutputStream outputStream = new DataOutputStream(socket
				.getOutputStream());
		outputStream.writeUTF(cmd);
		outputStream.flush();
	}

	/**
	 * 写文件
	 * 
	 * @param fileName
	 * @param fileSize
	 * @throws IOException
	 */
	public void writeFile(String fileName, long fileSize) throws IOException
	{
		Logger.debug("收到杂凑文件名" + fileName);
		fileName = fileName.trim();
		DataInputStream inputStream = null;
		DataOutputStream fileOut;
		byte[] buf = new byte[Config.BUFFER_SIZE];
		long counter = 0;

		/* 生成文件夹 */
		if (!new File(fileName).getParentFile().exists()) {
			new File(fileName).getParentFile().mkdirs();
		}

		inputStream = new DataInputStream(socket.getInputStream());
		fileOut = new DataOutputStream(new BufferedOutputStream(
				new FileOutputStream(fileName)));

		int flagCounter = 0;
		int remainSize = 0;
		while (counter < fileSize) {
			int read = 0;

			if (inputStream != null) {
				read = inputStream.read(buf);
			}

			flagCounter += read;

			if (flagCounter == Config.BUFFER_SIZE) {

				/* 返回客户端收到字节成功的信息 */
				reponseNext(flagCounter);
				flagCounter = 0;
			}

			counter += read;
			Logger.debug("COUNTER:"+counter);
			fileOut.write(buf, 0, read);
			remainSize = read;
		}

		reponseNext(remainSize);
		reponseNext(-1);

		fileOut.flush();
		fileOut.close();
	}

	/**
	 * 写文件
	 * 
	 * @param fileName
	 * @param startSize
	 * @throws IOException
	 */
	public void writeResumeFile(String fileName, long startSize)
			throws IOException
	{
		Logger.debug("收到需要续传的文件名" + fileName);
		fileName = fileName.trim();
		DataInputStream inputStream = null;
		byte[] buf = new byte[Config.BUFFER_SIZE];
		long counter = 0;

		/* 生成文件夹 */
		if (!new File(fileName).getParentFile().exists()) {
			new File(fileName).getParentFile().mkdirs();
		}

		inputStream = new DataInputStream(socket.getInputStream());
		RandomAccessFile file = new RandomAccessFile(fileName, "rw");
		file.seek(startSize);

		int flagCounter = 0;
		while (true) {
			int read = 0;

			if (inputStream != null) {
				read = inputStream.read(buf);
			} else {
				break;
			}
			
			if(read == -1) break;
			flagCounter += read;

			if (flagCounter == Config.BUFFER_SIZE) {

				/* 返回客户端收到字节成功的信息 */
				reponseNext(flagCounter);
				flagCounter = 0;
			}

			counter += read;
			Logger.debug("COUNTER:"+counter);
			file.write(buf, 0, read);
			// fileOut.write(buf, 0, read);
		}

		reponseNext(-1);

		file.seek(0);
		file.close();
	}

	/**
	 * 返回FLEX客户端NEXT信号
	 * 
	 * <p>
	 * 修正FLEX端的SOCKET BUG，其他形式客户端可不用
	 * </p>
	 * 
	 * @param fileSize
	 * @throws IOException
	 */
	public void reponseNext(long fileSize) throws IOException
	{
		System.out.println("不再发送NEXT");
		//response("<nmps><response>NEXT</response><fileSize>" + fileSize
		//		+ "</fileSize></nmps>");
	}

	/**
	 * 读文件
	 * 
	 * @param fileName
	 * @throws IOException
	 */
	public void readFile(String fileName) throws IOException
	{
		byte[] buf = new byte[Config.BUFFER_SIZE];
		long counter = 0;

		DataInputStream fileInputStream = null;
		DataOutputStream outputStream = null;

		outputStream = new DataOutputStream(socket.getOutputStream());
		fileInputStream = new DataInputStream(new BufferedInputStream(
				new FileInputStream(fileName)));

		while (true) {
			int read = 0;

			if (fileInputStream != null) {
				read = fileInputStream.read(buf);
			} else {
				break;
			}

			if (read == -1)
				break;
			counter += read;
			outputStream.write(buf, 0, read);
			outputStream.flush();
		}

		fileInputStream.close();
		outputStream.flush();
	}
}
