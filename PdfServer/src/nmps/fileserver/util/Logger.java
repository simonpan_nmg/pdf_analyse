package nmps.fileserver.util;

/**
 * 这是写LOG的类
 * 
 */
import java.io.*;

public class Logger
{
	private String	fileName;

	// private long fileSize;
	// private int counter;

	public Logger()
	{
		this.fileName = "logs/access.log";
		// this.fileSize = 200;
	}

	public void setFileName(String fileName)
	{
		this.fileName = fileName;
	}

	public void setFileSize(long size)
	{
		// this.fileSize = size;
	}

	public void write(String format, Object... args)
	{
		File file = new File(fileName);
		try {
			PrintWriter out = new PrintWriter(new BufferedWriter(
					new FileWriter(file, true)));
			out.printf(format, args);
			out.flush();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
		}
	}

	/*
	 * 调试指令
	 */
	public static void debug(String msg)
	{
		System.out.println(msg);
	}
	
	public static void debug(String msg, Throwable throwable)
	{	
		System.out.println(msg);
		throwable.printStackTrace();
	}
	
	public static void error(String msg)
	{
		System.out.println("ERROR>>>" + msg);
	}
	
	public static void error(String msg,  Throwable throwable)
	{
		System.out.println("ERROR>>>"+msg);
		throwable.printStackTrace();
	}
	
	public static void info(String msg)
	{
		System.out.println("INFO>>>" + msg);
	}
	
	public static void info(String msg,  Throwable throwable)
	{
		System.out.println("INFO>>>"+msg);
		throwable.printStackTrace();
	}
}
