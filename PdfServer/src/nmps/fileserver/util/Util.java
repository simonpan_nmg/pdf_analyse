package nmps.fileserver.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringWriter;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;

import com.sun.org.apache.xml.internal.serialize.OutputFormat;
import com.sun.org.apache.xml.internal.serialize.XMLSerializer;


public class Util {
	
	public static String getFileType(String fileName)
	{
		int pos;
		String ft = null;
		try {
			pos = fileName.lastIndexOf(".");
			ft = fileName.substring(pos);
		} catch(Exception e) {
		}
		return ft.toLowerCase();
	}
	
	 public static String getStringDate(String format)
	 {
		  Date currentTime = new Date();
		  if(format == null){
			  format = "yy-MM-dd HH:mm:ss";
		  }
		  SimpleDateFormat formatter = new SimpleDateFormat(format);
		  String dateString = formatter.format(currentTime);
		  return dateString;
	 }
	 
	 public static String randomString(int length)
	 {
		StringBuffer buffer = new StringBuffer("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"); 
		StringBuffer sb = new StringBuffer();
		
		Random r = new Random(); 
		int range = buffer.length();
		for (int i = 0; i < length; i ++)
		{ 
		    sb.append(buffer.charAt(r.nextInt(range))); 
		} 
		return sb.toString(); 
	 }
	 
	 public static String getTrimFile(String fileName, String flag)
	 {
		return fileName.replace("yy", flag); 
	 }
	 
	 public static void main(String[] args)
	 {
		 System.out.println(Util.getTrimFile("yy/MM/dd/",""));
	 }
	 
	 public static void copyFile(String in, String out) throws Exception 
	 {
		 System.out.println("COPY"+in+"->"+out);
		 FileInputStream fis  = new FileInputStream(in);
	     FileOutputStream fos = new FileOutputStream(out);
	     byte[] buf = new byte[1024];
	     int i = 0;
	     while((i=fis.read(buf))!=-1) {
	       fos.write(buf, 0, i);
	     }
	     fis.close();
	     fos.close();
	 }
	 
	 public static String DomToString(Document doc)
	 {
	        String STRXML = null;
	        try
	        {
	            OutputFormat format = new OutputFormat(doc);   //Serialize DOM
	            StringWriter stringOut = new StringWriter();        //Writer will be a String
	            XMLSerializer serial = new XMLSerializer(stringOut, format);
	            serial.asDOMSerializer();                     // As a DOM Serializer
	            serial.serialize(doc.getDocumentElement());
	            STRXML = stringOut.toString(); //Spit out DOM as a String
	        }
	        catch (Exception e)
	        {
	            e.printStackTrace();
	        }
	        return STRXML;
	 }
	 
	public static String readXML(String xmlPath)
	{
		String xml = "";
		try {		
			BufferedReader in = new BufferedReader(new FileReader(xmlPath));
			String s;
					
			while((s = in.readLine()) != null) {
				xml += s;
			}
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch(IOException e1){
			e1.printStackTrace();
		}
		return xml;
	}
	
	public static boolean doc2XmlFile(Document document,String filename) 
	{ 
		boolean flag = true; 
		try { 
			/** 将document中的内容写入文件中 */ 
			TransformerFactory tFactory = TransformerFactory.newInstance(); 
			Transformer transformer = tFactory.newTransformer(); 
			/** 编码 */ 
			//transformer.setOutputProperty(OutputKeys.ENCODING, "GB2312"); 
			DOMSource source = new DOMSource(document); 
			StreamResult result = new StreamResult(new File(filename)); 
			transformer.transform(source, result); 
		}catch(Exception ex) { 
			flag = false; 
			ex.printStackTrace(); 
		} 
		return flag; 
	}
	
	public static String getIP()
	{
		InetAddress addr;
		String ip =null;
		try {
			addr = InetAddress.getLocalHost();
			ip = addr.getHostAddress().toString();
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ip;
	}
}
