package nmps.fileserver.util;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

/**
 * 文件功能函数类
 * 
 * @author simonpan
 *
 */
public class FileUtil
{

	/**
	 * 获取文件类型
	 * 
	 * @param fileName 文件名
	 * @return
	 */
	public static String getFileType(String fileName)
	{
		int pos;
		String ft = null;
		try {
			pos = fileName.lastIndexOf(".");
			ft = fileName.substring(pos);
		} catch (Exception e) {
			ft = "";
		}
		return ft.toLowerCase();
	}
	
	/**
	 * 获取文件名（忽略文件类型）
	 * 
	 * @param fileName 文件名
	 * @return
	 */
	public static String getFileName(String fileName)
	{
		int pos;
		String ft = null;
		try {
			pos = fileName.lastIndexOf(".");
			ft = fileName.substring(0, pos);
		} catch (Exception e) {
			ft = "";
		}
		return ft.toString();
	}

	/**
	 * 获取当前日期
	 * 
	 * @param format 日期格式
	 * @return
	 */
	public static String  getStringDate(String format)
	{
		Date currentTime = new Date();
		if (format == null) {
			format = "yy-MM-dd HH:mm:ss";
		}
		SimpleDateFormat formatter = new SimpleDateFormat(format);
		String dateString = formatter.format(currentTime);
		return dateString;
	}

	/**
	 * 获取随机字符串
	 * 
	 * @param length 字符串长度
	 * @return
	 */
	public static String randomString(int length)
	{
		StringBuffer buffer = new StringBuffer(
				"0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ");
		StringBuffer sb = new StringBuffer();

		Random r = new Random();
		int range = buffer.length();
		for (int i = 0; i < length; i++) {
			sb.append(buffer.charAt(r.nextInt(range)));
		}
		return sb.toString();
	}

	/**
	 * 文件复制
	 * 
	 * @param source 源文件
	 * @param dest 目标文件
	 * @throws Exception
	 */
	public static void copyFile(String source, String dest) throws Exception
	{
		FileInputStream fis = new FileInputStream(source);
		FileOutputStream fos = new FileOutputStream(dest);
		byte[] buf = new byte[1024];
		int i = 0;
		while ((i = fis.read(buf)) != -1) {
			fos.write(buf, 0, i);
		}
		fis.close();
		fos.close();
	}
}
