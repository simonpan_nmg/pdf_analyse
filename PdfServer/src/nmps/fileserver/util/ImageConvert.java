package nmps.fileserver.util;


import java.io.*;

import nmps.fileserver.config.Config;


/**
 * 文件服务器图片转换
 * 
 * <p>在运行本类之前，要确定服务器安装了ImageMagick</p>
 * 
 * @author simonpan
 * */

public class ImageConvert {
    
    public static void resize(String file) {
        try {
        	if(file.lastIndexOf(".") == -1) return; 
        	String fileType = file.substring(file.lastIndexOf(".")+1);
           String fileName = file.substring(0,file.lastIndexOf("."));
           String thumbFile = Config.THUMB_PATH + "" + fileName.replace(Config.FILE_PATH, "");
           String lowFile = Config.LOW_PATH + "" + fileName.replace(Config.FILE_PATH, "");
           
		   thumbFile = thumbFile + ".jpg";
		   lowFile = lowFile + ".jpg";
           
           File baseDir = new File(thumbFile.substring(0, thumbFile.lastIndexOf("/")));
           File baseDir2 = new File(lowFile.substring(0, lowFile.lastIndexOf("/")));
           if(!baseDir.exists()) baseDir.mkdirs();
           if(!baseDir2.exists()) baseDir2.mkdirs();
           
           if(inPsdArray(fileType)) {
        	   execute("convert " +file + " -flatten -strip -resize 50%"+ "" +" -quality " + Config.PICTURE_QUALITY + " -colorspace rgb "+ lowFile);
        	   execute("convert " +lowFile + " -flatten -strip -resize 97x122  -quality " + Config.PICTURE_QUALITY + " -colorspace rgb "+ thumbFile);
           } else if(inJpegArray(fileType)) {
               execute("convert " + file + " -flatten -resize 50% -colorspace rgb " + lowFile);
               execute("convert " + lowFile + " -flatten -strip -resize 97x122  -quality " + Config.PICTURE_QUALITY+" -colorspace rgb " + thumbFile);
           } else {
        	   return;
           }
           
        } catch(Exception e) {
                 e.printStackTrace();
        }
    }
    
    public static void execute(String cmd) {
        try {
        	Process ps = Runtime.getRuntime().exec(cmd);//in WINos
        	loadStream(ps.getInputStream()); //in a block status
        	//Runtime.getRuntime().exec("cmd " + cmd);
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
    
    public static String executeReturn(String cmd) {
    	String str = null;
    	try {
    		Process ps = Runtime.getRuntime().exec(cmd);//in WINos
    		str = loadStream(ps.getInputStream()); //in a block status
    		//Runtime.getRuntime().exec("cmd " + cmd);
    	} catch (IOException e) {
    		e.printStackTrace();
    	}
    	return str;
    }
    
	private static String loadStream(InputStream in) throws IOException {
		int ptr = 0;
		in = new BufferedInputStream(in);
		StringBuffer buffer = new StringBuffer();
		while ((ptr = in.read()) != -1) {
			buffer.append((char) ptr);
		}
		return buffer.toString();
	}
	
    private static boolean inPsdArray(String type)
    {
    	String[] fileType = {"psd", "tif", "tiff"};
    	return inArray(type, fileType);    	
    }
    
    private static boolean inJpegArray(String type)
    {
    	String[] fileType = {"jpg", "jpeg", "gif", "png", "bmp"};
    	return inArray(type, fileType);
    }
    
    private static boolean inArray(String type, String[] fileArray)
    {
    	for(int i=0; i<fileArray.length; i++) {
    		if(type.equalsIgnoreCase(fileArray[i])) return true;
    	}
    	return false;
    }
}

