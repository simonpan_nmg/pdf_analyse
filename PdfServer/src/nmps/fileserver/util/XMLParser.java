package nmps.fileserver.util;

import java.io.IOException;
import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class XMLParser
{
	/**
	 * 获取XML一个节点的值
	 *
	 * @param xml
	 * @param filed
	 * @return
	 */
	public static String parseXML(String xml, String nodeName)
	{
		StringReader sr = new StringReader(xml);
		InputSource is = new InputSource(sr);
		/* parse xml */
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder;

		String cmd = null;
		try {
			builder = factory.newDocumentBuilder();
			Document doc = null;
			try {
				doc = builder.parse(is);
			} catch (SAXException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			Element rootElement = doc.getDocumentElement();
			cmd = rootElement.getElementsByTagName(nodeName).item(0)
					.getTextContent();

		} catch (ParserConfigurationException e) {
			// e.printStackTrace();
		}
		return cmd;

	}
}
