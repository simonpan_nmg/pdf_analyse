package nmps.fileserver.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.InvalidPropertiesFormatException;
import java.util.Properties;

/**
 * 读取指令对应类
 * 
 * @author simonpan
 * 
 * */
public class ServerProperties
{
	public static String loadCommad(String cmd)
			throws InvalidPropertiesFormatException, FileNotFoundException,
			IOException
	{
		Properties p = new Properties();
		String mapFile = "class-map.xml";

		/* 兼容开发环境和打包JAR文件 */
		File f = new File(mapFile); //默认打包环境
		if (!f.exists()) {
			f = new File("../" + mapFile); //开发环境
		}
		
		String fixClass = null;
		p.loadFromXML(new FileInputStream(f));
		fixClass = p.getProperty(cmd);
		if (null == fixClass)
			return "";
		return fixClass;
	}
}
