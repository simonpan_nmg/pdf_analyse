package nmps.fileserver.state.download;

import java.io.File;
import java.io.IOException;
import java.net.Socket;

import nmps.fileserver.util.DataTransportor;
import nmps.fileserver.util.FileUtil;
import nmps.fileserver.util.Logger;
import nmps.fileserver.util.XMLParser;

public class DownloadFile
{
	private Socket	socket;
	public String	command;
	public String	fileName;
	public int		fileSize;
	public String downloadPath;
	public boolean photoFlag = false;

	/**
	 * @param socket
	 */
	public DownloadFile(Socket socket)
	{
		this.socket = socket;
	}

	/**
	 * 处理下载
	 * 
	 * @return 如果成功返回1, 如果不成功返回-1,跳过用0
	 * @throws Exception 
	 */
	public int doDownload() throws Exception
	{
		init();
		if (this.command.equals("END")) {
			response("ALL_OK", "");
			return -1;
		}
		
		fileName = downloadPath + fileName;

		if(photoFlag) {
			fileName = FileUtil.getFileName(fileName) + ".jpg";
		}
		System.out.println("需要下载的低清图是"+fileName);
		
		if(new File(fileName).exists()){
			
			/*文件存在就返回成功信息*/
			response("READY", fileName, new File(fileName).length());
		} else {
			
			/*文件存在就返回成功信息*/
			response("INVALID", fileName, 0);
			return 0;
		}
		
		if(!ready()) return -1;
		
		try {
			new DataTransportor(socket).readFile(fileName);
			Logger.debug("文件" + fileName + "成功下载");
		} catch (IOException e) {
			Logger.error("文件" + fileName + "下载失败");
			throw e;
		}
		
		return 1;
	}

	private void init()
	{
		String xml;
		try {
			xml = new DataTransportor(socket).request();
			this.command = XMLParser.parseXML(xml, "command");
			if (!command.equals("END")) {
				this.fileName = XMLParser.parseXML(xml, "fileName");
			} else {
				//response("ALL_OK", fileName);
			}
		} catch (IOException e) {
			response("FAIL", fileName);
			Logger.error("读取文件名出错", e);
		}
	}
	
	private boolean ready()
	{
		String xml;
		try {
			xml = new DataTransportor(socket).request();
			String command = XMLParser.parseXML(xml, "command");
			if (command.equals("READY")) {
				return true;
			}
		} catch (IOException e) {
			Logger.error("接收READY指令出错", e);
			return false;
		}
		return false;
		
	}

	private void response(String msg, String fileName)
	{
		try {
			new DataTransportor(socket)
					.response("<nmps><response>" + msg
							+ "</response><fileName>" + fileName
							+ "</fileName></nmps>");
		} catch (IOException e) {
			Logger.error("文件下载过程发送信息失败", e);
		}
	}

	/**
	 * 返回数据
	 * 
	 * @param status
	 * @param fileName
	 * @param fileSize
	 */
	private void response(String status, String fileName, long fileSize)
	{
		try {
			new DataTransportor(socket).response("<nmps><response>" + status
					+ "</response>" + "<fileName>" + fileName + "</fileName>"
					+ "<fileSize>" + fileSize + "</fileSize>" + "</nmps>");
		} catch (IOException e) {
			Logger.error("文件下载过程发送信息失败", e);
		}
	}
}
