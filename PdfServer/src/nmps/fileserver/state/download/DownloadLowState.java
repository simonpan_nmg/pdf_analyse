package nmps.fileserver.state.download;

import java.io.IOException;
import java.net.Socket;

import nmps.fileserver.Context;
import nmps.fileserver.IState;
import nmps.fileserver.config.Config;
import nmps.fileserver.state.load.LoadState;
import nmps.fileserver.util.DataTransportor;
import nmps.fileserver.util.Logger;

/**
 * 下载状态类
 * 
 * @author simonpan
 * 
 */
public class DownloadLowState implements IState
{
	private Socket	socket;

	/*
	 * (non-Javadoc)
	 * 
	 * @see nmps.fileserver.IState#handle(nmps.fileserver.Context)
	 */
	@Override
	public void handle(Context context)
	{
		this.socket = context.socket;
		response("ACCEPT", null);
		try {
			run();
			Logger.debug("文件全部下载完毕");

			/* 把状态主动权交给LoadState */
			context.socket = socket;
			context.state = new LoadState();
			context.handle();
		} catch (Exception e) {
			Logger.error("线程在DOWNLOAD低清图状态遇上异常中断");
		}

	}

	/**
	 * 循环处理文件
	 * 
	 * @throws Exception
	 */
	public void run() throws Exception
	{
		/* 循环文件，直到收到停止的信号 */
		while (true) {
			DownloadFile downloadFile = new DownloadFile(socket);
			downloadFile.downloadPath = Config.LOW_PATH;
			downloadFile.photoFlag = true;
			int signal = downloadFile.doDownload();
			if (signal == -1)
				break;
		}
	}
	
	
	/**
	 * 返回信息给客户端
	 * 
	 * @param msg
	 * @param fileName
	 */
	private void response(String msg, String fileName)
	{
		try {
			new DataTransportor(socket)
					.response("<nmps><response>" + msg
							+ "</response><fileName>" + fileName
							+ "</fileName></nmps>");
		} catch (IOException e) {
			Logger.error("文件低清图处理过程发送信息失败", e);
		}
	}
}
