package nmps.fileserver.state.stop;

import java.io.IOException;
import java.net.Socket;

import nmps.fileserver.Context;
import nmps.fileserver.IState;
import nmps.fileserver.util.DataTransportor;
import nmps.fileserver.util.Logger;

/**
 * 退出状态类
 * 
 * @author simonpan
 *
 */
public class StopState implements IState
{
	/** SOCKET */
	private Socket	socket;
	
	@Override
	public void handle(Context context)
	{
		Logger.debug(context.socket + "选择离开, ByeBye!");
		socket = context.socket;
		response("BYE", null);
	}
	
	/**
	 * 返回信息给客户端
	 * 
	 * @param msg
	 * @param fileName
	 */
	private void response(String msg, String fileName)
	{
		try {
			new DataTransportor(socket)
					.response("<nmps><response>" + msg
							+ "</response><fileName>" + fileName
							+ "</fileName></nmps>");
		} catch (IOException e) {
			Logger.error("文件处理过程发送信息失败", e);
		}
	}

}
