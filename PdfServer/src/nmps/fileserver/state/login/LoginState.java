package nmps.fileserver.state.login;

import java.io.IOException;
import java.net.Socket;

import nmps.fileserver.Context;
import nmps.fileserver.IState;
import nmps.fileserver.config.Config;
import nmps.fileserver.state.load.LoadState;
import nmps.fileserver.util.DataTransportor;
import nmps.fileserver.util.Logger;
import nmps.fileserver.util.XMLParser;

/**
 * 登陆状态类
 * 
 * @author simonpan
 *
 */
public class LoginState implements IState
{
	/** SOCKET*/
	private Socket	socket;

	/* (non-Javadoc)
	 * @see fileserver.IState#handle(fileserver.Context)
	 */
	@Override
	public void handle(Context context)
	{
		this.socket = context.socket;
		String xml = null;

		try {
			xml = new DataTransportor(socket).request();
		} catch (IOException e) {
			Logger.error("读取UTF指令代码错误" + xml, e);
		}

		String command = XMLParser.parseXML(xml, "command");
		String username = XMLParser.parseXML(xml, "username");
		String password = XMLParser.parseXML(xml, "password");

		if (command.equals("LOGIN") && password.equals(Config.PSW)) {

			/* 返回客户端成功登陆的信息 */
			response("SUCCESS", "OK");

			context.state = new LoadState();
			context.socket = socket;
			context.handle();
		} else {
			Logger.error(username + "使用" + password + "非法进入");

			/* 返回客户端登陆失败的信息 */
			response("FAIL", "ERROR PASSWORD");
		}
	}

	/**
	 * 返回信息
	 * 
	 * @param status 状态信息
	 * @param msg 返回信息
	 */
	private void response(String status, String msg)
	{
		try {
			new DataTransportor(socket).response("<nmps><response>" + status
					+ "</response></nmps>");
		} catch (IOException e) {
			Logger.error("发送登陆信息失败", e);
		}
	}

}
