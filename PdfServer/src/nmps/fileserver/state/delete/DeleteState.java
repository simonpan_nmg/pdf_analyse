package nmps.fileserver.state.delete;

import java.io.File;
import java.io.IOException;
import java.net.Socket;

import nmps.fileserver.Context;
import nmps.fileserver.IState;
import nmps.fileserver.config.Config;
import nmps.fileserver.state.load.LoadState;
import nmps.fileserver.util.DataTransportor;
import nmps.fileserver.util.Logger;
import nmps.fileserver.util.XMLParser;

/**
 * 删除文件状态状态类
 * 
 * @author simonpan
 *
 */
public class DeleteState implements IState
{
	private Socket	socket;

	@Override
	public void handle(Context context)
	{
		this.socket = context.socket;
		response("ACCEPT", null);
		try {
			run();

			/* 把状态主动权交给LoadState */
			context.socket = socket;
			context.state = new LoadState();
			context.handle();
		} catch (Exception e) {
			Logger.error("线程在UPLOAD状态遇上异常中断", e);
		}
	}

	/**
	 * 运行循环读取文件
	 * 
	 * @throws Exception
	 */
	public void run() throws Exception
	{
		String xml = new DataTransportor(socket).request();
		String file = XMLParser.parseXML(xml, "fileName");
		Logger.debug("文件" + file + "正在删除");
		// 实际文件删除
		new File(Config.FILE_PATH + file).delete();
		// 大图删除
		new File(Config.LOW_PATH + file).delete();
		// 缩略图删除
		new File(Config.THUMB_PATH + file).delete();
	}
	
	/**
	 * 返回信息给客户端
	 * 
	 * @param msg
	 * @param fileName
	 */
	private void response(String msg, String fileName)
	{
		try {
			new DataTransportor(socket)
					.response("<nmps><response>" + msg
							+ "</response><fileName>" + fileName
							+ "</fileName></nmps>");
		} catch (IOException e) {
			Logger.error("文件处理过程发送信息失败", e);
		}
	}
}
