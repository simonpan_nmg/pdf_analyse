package nmps.fileserver.state.load;

import java.io.IOException;
import java.net.Socket;

import nmps.fileserver.Context;
import nmps.fileserver.Factory;
import nmps.fileserver.IState;
import nmps.fileserver.util.DataTransportor;
import nmps.fileserver.util.Logger;
import nmps.fileserver.util.ServerProperties;


/**
 * 加载状态类
 * 
 * @author simonpan
 *
 */
public class LoadState implements IState
{
	private Socket	socket;

	/* (non-Javadoc)
	 * @see fileserver.IState#handle(fileserver.Context)
	 */
	@Override
	public void handle(Context context)
	{
		this.socket = context.socket;
		IState state = loadStatus();
		if (null != state) {
			context.state = state;
			context.socket = socket;
			context.handle();
		}
	}

	/**
	 * 获取指令对应状态
	 * 
	 * <p>根据配置文件获取对应的类，若需扩展，修改配置文件即可</p>
	 * 
	 * @return 返回状态
	 */
	private IState loadStatus()
	{
		String cmd = null;
		String cls = null;

		try {
			cmd = new DataTransportor(socket).requestCommand();
			cls = ServerProperties.loadCommad(cmd);
			IState factory = Factory.getState(cls);
			
			return factory;
		} catch (IOException e) {
			/* 返回客户端失败的信息 */
			response("DENY");
			Logger.error("读取UTF指令代码错误" + cmd, e);
		} catch (ClassNotFoundException e) {
			response("DENY");
			Logger.error("找不到类" + cmd, e);
		} catch (InstantiationException e) {
			response("DENY");
			Logger.error("入口查找类异常" + cmd, e);
		} catch (IllegalAccessException e) {
			response("DENY");
			Logger.error("入口查找类异常" + cmd, e);
		} catch (Exception e) {
			response("DENY");
			Logger.error("异常的处理命令" + cmd, e);
		}
		return null;

	}

	/**
	 * 返回信息
	 * 
	 * @param status
	 */
	private void response(String status)
	{
		try {
			new DataTransportor(socket).response("<nmps><response>" + status
					+ "</response></nmps>");
		} catch (IOException e) {
			Logger.error("LOAD状态发送信息失败", e);
		}
	}
}
