package nmps.fileserver.state.upload;

import java.io.IOException;
import java.net.Socket;

import nmps.fileserver.Context;
import nmps.fileserver.IState;
import nmps.fileserver.util.DataTransportor;
import nmps.fileserver.util.Logger;

/**
 * 上传广告稿状态类
 * 
 * @author simonpan
 * 
 * */
public class UploadAdState extends UploadState implements IState
{
	private Socket socket;
	/* (non-Javadoc)
	 * @see nmps.fileserver.state.upload.UploadState#handle(nmps.fileserver.Context)
	 */
	@Override
	public void handle(Context context)
	{
		socket = context.socket;
		response("ACCEPT", null);
		super.handle(context);
	}
	
	/**
	 * 覆盖父类转换的方法，广告稿不需要转换
	 * 
	 * @see nmps.fileserver.state.upload.UploadState#convert()
	 */
	public void convert()
	{
		Logger.debug("广告稿不用转换");
	}
	
	/**
	 * 返回信息给客户端
	 * 
	 * @param msg
	 * @param fileName
	 */
	private void response(String msg, String fileName)
	{
		try {
			new DataTransportor(socket)
					.response("<nmps><response>" + msg
							+ "</response><fileName>" + fileName
							+ "</fileName></nmps>");
		} catch (IOException e) {
			Logger.error("接受广告指令过程中发送信息失败", e);
		}
	}
}
