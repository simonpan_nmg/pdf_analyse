package nmps.fileserver.state.upload;

import nmps.fileserver.Context;
import nmps.fileserver.IState;
import nmps.fileserver.util.Logger;

/**
 * 上传PDF稿状态类
 * 
 * @author simonpan
 * 
 * */
public class UploadPdfState extends UploadState implements IState
{

	/* (non-Javadoc)
	 * @see nmps.fileserver.state.upload.UploadState#handle(nmps.fileserver.Context)
	 */
	@Override
	public void handle(Context context)
	{
		super.handle(context);
	}
	
	/**
	 * 覆盖父类转换的方法，PDF不需要转换
	 * 
	 * @see nmps.fileserver.state.upload.UploadState#convert()
	 */
	public void convert()
	{
		Logger.debug("pdf稿不用转换");
	}
}
