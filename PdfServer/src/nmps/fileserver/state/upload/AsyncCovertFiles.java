package nmps.fileserver.state.upload;

import java.util.Iterator;
import java.util.Vector;

import nmps.fileserver.config.Config;
import nmps.fileserver.util.ImageConvert;
import nmps.fileserver.util.Logger;

/**
 * 异步转换文件
 * 
 * @author simonpan
 * 
 * */

public class AsyncCovertFiles extends Thread
{
	Vector<String>	filesList	= new Vector<String>();

	public AsyncCovertFiles(Vector<String> filesList)
	{
		this.filesList = filesList;
	}

	public void run()
	{
		Iterator<String> it = filesList.iterator();
		while (it.hasNext()) {
			String file = it.next();
			if (null == file)
				continue;
			ImageConvert.resize(Config.FILE_PATH + file);
			Logger.debug("文件" + file + "已被转换");
		}
	}

}
