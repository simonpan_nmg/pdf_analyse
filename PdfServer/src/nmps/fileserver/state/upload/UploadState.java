package nmps.fileserver.state.upload;

import java.io.File;
import java.io.IOException;
import java.net.Socket;
import java.util.Iterator;
import java.util.Vector;

import nmps.fileserver.Context;
import nmps.fileserver.IState;
import nmps.fileserver.config.Config;
import nmps.fileserver.state.load.LoadState;
import nmps.fileserver.util.DataTransportor;
import nmps.fileserver.util.Logger;

/**
 * 上传状态类
 * 
 * @author simonpan
 *
 */
public class UploadState implements IState
{
	private Socket	socket;

	Vector<String>	filesList	= new Vector<String>();

	@Override
	public void handle(Context context)
	{
		this.socket = context.socket;
		try {
			run();
			Logger.debug("文件全部上传完毕");
			Logger.debug("开始转换文件");

			/* 异步转换 */
			convert();

			/* 把状态主动权交给LoadState */
			context.socket = socket;
			context.state = new LoadState();
			context.handle();
		} catch (Exception e) {
			Logger.error("线程在UPLOAD状态遇上异常中断", e);
		}
	}

	/**
	 * 运行循环读取文件
	 * 
	 * @throws Exception
	 */
	public void run() throws Exception
	{
		/* 循环文件，直到收到停止的信号 */
		while (true) {
			UploadFile uploadFile = new UploadFile(socket);

			int signal;
			try {
				signal = uploadFile.doUpload();
				if (signal == -1)
					break;
				filesList.add(uploadFile.randFileName);
			} catch (Exception e) {
				filesList.add(uploadFile.randFileName);
				remove();
				throw e; // 抛出异常
			}
		}
	}

	/**
	 * 异步转换
	 */
	public void convert()
	{
		Logger.debug("异步转换");
		Thread thread = new Thread(new AsyncCovertFiles(filesList));
		thread.start();
	}

	/**
	 * 删除文件 
	 * 
	 * <p>在发生任何异常情况下，删除之前已上传成功的文件</p>
	 */
	public void remove()
	{
		Iterator<String> it = filesList.iterator();
		while (it.hasNext()) {
			String file = it.next();
			if (null == file)
				continue;
			Logger.debug("文件" + file + "正在删除");
			new File(Config.FILE_PATH + file).delete(); // 删除文件
		}
	}
	
	/**
	 * 返回信息给客户端
	 * 
	 * @param msg
	 * @param fileName
	 */
	private void response(String msg, String fileName)
	{
		try {
			new DataTransportor(socket)
					.response("<nmps><response>" + msg
							+ "</response><fileName>" + fileName
							+ "</fileName></nmps>");
		} catch (IOException e) {
			Logger.error("接受普通文件上传指令过程中发送信息失败", e);
		}
	}

}
