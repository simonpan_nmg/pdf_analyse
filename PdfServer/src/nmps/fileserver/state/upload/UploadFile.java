package nmps.fileserver.state.upload;

import java.io.IOException;
import java.net.Socket;

import nmps.fileserver.config.Config;
import nmps.fileserver.util.DataTransportor;
import nmps.fileserver.util.FileUtil;
import nmps.fileserver.util.Logger;
import nmps.fileserver.util.XMLParser;

public class UploadFile
{
	/** SOCKET */
	private Socket	socket;

	/** 命令 */
	public String	command;

	/** 文件名 */
	public String	fileName;

	/** 杂凑文件名 */
	public String	randFileName;

	/** 文件大小 */
	public long		fileSize;

	public UploadFile(Socket socket)
	{
		this.socket = socket;
	}

	/**
	 * 处理上传
	 * 
	 * @return
	 * @throws Exception 
	 */
	public int doUpload() throws Exception
	{
		init();
		if (this.command.equals("END")) {
			return -1;
		}

		try {
			new DataTransportor(socket).writeFile(Config.FILE_PATH
					+ randFileName, fileSize);

			/* 返回成功信息 */
			Logger.debug("文件" + fileName + "已经上传");
			//new DataTransportor(socket).response("HELLO");
		} catch (IOException e) {

			/* 返回失败信息 */
			Logger.error("文件" + fileName + "传输失败", e);
			throw e;
		}
		return 1;
	}

	/**
	 * 初始化类属性
	 */
	private void init()
	{
		String xml;
		try {
			xml = new DataTransportor(socket).request();
			Logger.debug(xml);
			this.command = XMLParser.parseXML(xml, "command");
			
			
			if(command.equals("END")) {
				/* 收到信息后反馈给客户端 */
				response("ALL_OK", fileName);
			} else{
				this.fileName = XMLParser.parseXML(xml, "fileName").trim();
				this.fileSize = Long.parseLong(XMLParser.parseXML(xml,
						"fileSize"));

				/* 杂凑文件名 */
				randFileName = makeRandFileName();
				/* 收到信息后反馈给客户端 */
				response("READY", randFileName);
			}
			
		} catch (IOException e) {
			response("INVALID", fileName);
			Logger.error("读取文件名出错", e);
		} catch (Exception e) {
			response("INVALID", fileName);
			Logger.error("致命错误");
		}
	}

	/**
	 * 返回信息给客户端
	 * 
	 * @param msg
	 * @param fileName
	 */
	private void response(String msg, String fileName)
	{
		try {
			new DataTransportor(socket)
					.response("<nmps><response>" + msg
							+ "</response><fileName>" + fileName
							+ "</fileName></nmps>");
		} catch (IOException e) {
			Logger.error("文件处理过程发送信息失败", e);
		}
	}

	/**
	 * 杂凑文件名
	 * 
	 * <p>
	 * 按 <b>年/月/日/时/</b> 归档
	 * </p>
	 * 
	 * @return 返回杂凑字符
	 */
	public String makeRandFileName()
	{
		String fileType = FileUtil.getFileType(fileName);
		String folder = FileUtil.getStringDate("yy/MM/dd/HH/");
		return folder + FileUtil.randomString(32) + fileType.trim();
	}
}
