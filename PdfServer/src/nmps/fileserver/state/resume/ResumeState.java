package nmps.fileserver.state.resume;

import java.net.Socket;
import java.util.Vector;

import nmps.fileserver.Context;
import nmps.fileserver.IState;
import nmps.fileserver.state.load.LoadState;
import nmps.fileserver.util.Logger;

/**
 * 上传状态类
 * 
 * @author simonpan
 *
 */
public class ResumeState implements IState
{
	private Socket	socket;

	Vector<String>	filesList	= new Vector<String>();

	@Override
	public void handle(Context context)
	{
		this.socket = context.socket;
		try {
			run();
			Logger.debug("文件全部续传完毕");
			Logger.debug("开始转换文件");

			/* 把状态主动权交给LoadState */
			context.socket = socket;
			context.state = new LoadState();
			context.handle();
		} catch (Exception e) {
			Logger.error("线程在RESUME状态遇上异常中断", e);
		}
	}

	/**
	 * 运行循环读取文件
	 * 
	 * @throws Exception
	 */
	public void run() throws Exception
	{
		/* 循环文件，直到收到停止的信号 */
		while (true) {
			ResumeFile resumeFile = new ResumeFile(socket);

			int signal;
			try {
				signal = resumeFile.doResume();
				if (signal == -1)
					break;
			} catch (Exception e) {
				throw e; // 抛出异常
			}
		}
	}
}
