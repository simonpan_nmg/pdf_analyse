package nmps.fileserver.state.resume;

import java.io.File;
import java.io.IOException;
import java.net.Socket;

import nmps.fileserver.config.Config;
import nmps.fileserver.util.DataTransportor;
import nmps.fileserver.util.Logger;
import nmps.fileserver.util.XMLParser;

public class ResumeFile
{
	/** SOCKET */
	private Socket	socket;

	/** 命令 */
	public String	command;

	/** 文件名 */
	public String	fileName;

	/** 杂凑文件名 */
	public String	randFileName;

	/** 文件大小 */
	public long		fileSize;

	/** 断点位置 */
	public long		startSize;

	public ResumeFile(Socket socket)
	{
		this.socket = socket;
	}

	/**
	 * 处理上传
	 * 
	 * @return
	 * @throws Exception
	 */
	public int doResume() throws Exception
	{
		init();
		if (this.command.equals("END")) {
			return -1;
		}

		try {
			new DataTransportor(socket).writeResumeFile(Config.FILE_PATH
					+ fileName, startSize);

			/* 返回成功信息 */
			Logger.debug("文件" + fileName + "已经上传");
			// new DataTransportor(socket).response("HELLO");
		} catch (IOException e) {

			/* 返回失败信息 */
			Logger.error("文件" + fileName + "传输失败", e);
			throw e;
		}
		return 1;
	}

	/**
	 * 初始化类属性
	 */
	private void init()
	{
		String xml;
		try {
			xml = new DataTransportor(socket).request();
			Logger.debug(xml);
			this.command = XMLParser.parseXML(xml, "command");

			if (command.equals("END")) {
				/* 收到信息后反馈给客户端 */
				response("ACCEPT", fileName, startSize);
			} else {
				this.fileName = XMLParser.parseXML(xml, "fileName").trim();
				this.fileSize = Long.parseLong(XMLParser.parseXML(xml,
						"fileSize"));

				/* 收到信息后反馈给客户端 */
				this.startSize = new File(Config.FILE_PATH + fileName).length();
				response("READY", fileName, startSize);
			}

		} catch (IOException e) {
			response("INVALID", fileName, startSize);
			Logger.error("读取文件名出错", e);
		} catch (Exception e) {
			response("INVALID", fileName, startSize);
			Logger.error("致命错误");
		}
	}

	/**
	 * 返回信息给客户端
	 * 
	 * @param msg
	 * @param fileName
	 */
	private void response(String msg, String fileName, long startSize)
	{
		try {
			new DataTransportor(socket)
					.response("<nmps><response>" + msg
							+ "</response><fileName>" + fileName
							+ "</fileName><startSize>"+startSize+"</startSize></nmps>");
		} catch (IOException e) {
			Logger.error("文件续传处理过程发送信息失败", e);
		}
	}
}
