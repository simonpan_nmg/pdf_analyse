package nmps.fileserver;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;

import nmps.fileserver.config.Config;
import nmps.fileserver.util.Logger;
import nmps.fileserver.util.Util;

public class Test
{
	public static long fileCounter = 0;
	private static ArrayList fileListArray;

	private static int imageCounter = 1;

//	static String folder = "D:\\wamp\\www\\pdf-verification\\photo\\";
	static String folder = "./photo/";
	
	public static void doTest(Socket socket)
	{
		System.out.println(socket);
		fileCounter = 0;
		fileListArray = new ArrayList();
//		Timestamp   unixTime   =   new   Timestamp();
//		long startTime = unixTime.getDateTime();
		
		DataInputStream inputStream = null;
		DataOutputStream outputStream = null;
		BufferedReader in = null;
		PrintWriter out = null;
		try {
			inputStream = new DataInputStream(socket
					.getInputStream());
			outputStream = new DataOutputStream(socket
					.getOutputStream());
			in = new BufferedReader(new InputStreamReader(inputStream, "ISO-8859-1"));
			out = new PrintWriter(new OutputStreamWriter(outputStream, "ISO-8859-1"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			while(true){
				String fileName = inputStream.readUTF();
				if(fileName.equals("GET_INFO")) {
					// 数组的长度
					// 读取刚才上传的PDF
					String pdf = folder + (String) fileListArray.get(0);
					System.out.println("PDF:"+pdf);
					String info = "";
					try {
						ExtractImages extractor = new ExtractImages();
						info = extractor.extractImages(pdf);
						System.out.println("INFO"+info);

						info = info.substring(0, info.length()-1);
						

//						System.out.println("INFO_LENGTH:"+info);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						fileListArray.clear();
						break;
					}
					
//					System.out.println(fileListArray.size());
					outputStream.writeUTF(info);
					outputStream.flush();
					continue;
				}
				if(fileName.equals("END")) {
					fileListArray.clear();
					break;
				}
				String randFileName = Util.randomString(5)+""+Util.getFileType(fileName.split("\\|")[0]);
				System.out.println("ramd:"+randFileName+"]");
				fileListArray.add(randFileName);
				writeFile(socket, randFileName+"|"+fileName.split("\\|")[1]);
				//发送完成指令
				outputStream.writeUTF("OK");
				outputStream.flush();
			}
//			out.write("{cmd:'STOP'");
//			out.flush();
//			out.write("simon\n\n");
//			out.flush();
//			int read = inputStream.read(buf);
//			String abc = new String(buf);
//			System.out.println("STR_1=>"+str_1);
//			String str_2 = in.readLine();
//			strToFile(str_2);
//			str_2 = new  String(str_2.getBytes("ISO-8859-1"),"GBK");  
//			System.out.println("STR_2=>"+str_2);


			
//			long endTime = new Timestamp().getDateTime();
//			long timeUsed = (long)(endTime-startTime+1)/1000;
//			long speed = (long) fileCounter / (timeUsed+1);
//			System.out.println("BYE "+ "Time: "+timeUsed+" bps: "+speed);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			
			e.printStackTrace();
		} finally{
			try {
				socket.close();
				System.out.println("FINALLY CLOSE");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public static void strToFile(String s)
	{
		PrintWriter pw = null;
		try {
			pw = new PrintWriter(new OutputStreamWriter(new FileOutputStream("d:\\tmp\\hello3.txt")),true);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		pw.println(s);
		
	}
	
	/**
	 * 写文件
	 * 
	 * @param fileName
	 * @param fileSize
	 * @throws IOException
	 */
	public static void writeFile(Socket socket, String comingName) throws IOException
	{
//		String folder = "./Normal/";
		// String folder = "/home/web/nmps/Files/simonpan/";
		String[] arr = comingName.split("\\|");
//		System.out.println("COMMING NAME IS "+arr[1]+"|"+arr[0]);
		String fileName = folder+arr[0];
		long fileSize = Long.parseLong(arr[1]);
		//String fileName = "/home/web/nmps/mytest_simon.txt";
		
		DataInputStream inputStream = null;
		DataOutputStream fileOut;
		byte[] buf = new byte[Config.BUFFER_SIZE];
		long counter = 0;

		/* 生成文件夹 */
		File baseFolder = new File(fileName).getParentFile();
		if (!baseFolder.exists()) {
			baseFolder.mkdirs();
			baseFolder.getParentFile().getParentFile().getParentFile().setExecutable(true, false);  // 第四层（文件类型）

			baseFolder.getParentFile().getParentFile().setExecutable(true, false);  // 第三层（年）

			baseFolder.getParentFile().setExecutable(true, false);  // 第二层（月）

			baseFolder.setExecutable(true, false); // 第一层 （日）
		}
		
		inputStream = new DataInputStream(socket.getInputStream());
		fileOut = new DataOutputStream(new BufferedOutputStream(
				new FileOutputStream(fileName)));

		int flagCounter = 0;
		while (counter<fileSize) {
			int read = 0;

			if (inputStream != null) {
				read = inputStream.read(buf);
				if(read == -1) break;
			}

			flagCounter += read;

			counter += read;
			fileCounter += read;
			Logger.debug("COUNTER:"+counter);
			fileOut.write(buf, 0, read);
		}
		System.out.println("完成了");
		fileOut.flush();
		fileOut.close();
	}
}
