package nmps.fileserver;

import java.net.Socket;

/**
 * 上下文类
 * 
 * @author simonpan
 *
 */
public class Context
{
	public IState	state;
	public String	cmd;
	public Socket	socket;

	/**
	 * @param state 状态类
	 */
	public Context(IState state)
	{
		this.state = state;
	}

	/**
	 * 处理
	 */
	public void handle()
	{
		state.handle(this);
	}
}
