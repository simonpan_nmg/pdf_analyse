import java.applet.Applet;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Panel;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;

import javax.swing.JFileChooser;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.filechooser.FileFilter;

import netscape.javascript.JSObject;

import com.sun.jmx.snmp.Timestamp;

public class pdfApplet extends Applet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	boolean isStandalone = false;
	private Socket client = null;
//	private String host = "192.168.2.3";
//	private String host = "nmpsport.nmg.com.hk";
	private String host = "bpmdev1.nmg.com.hk";
	private boolean debug = false;

	private ArrayList fileListArray = new ArrayList();

	DataOutputStream outputStream = null;
	DataInputStream inputStream = null;
	private int port = 1024;

	private long fileCounter = 0;
	
	static JSObject window;
	
	private String matchResponse = null;
	
	private String mode = "single";
	

	// Get a parameter value
	public String getParameter(String key, String def) {
		return isStandalone ? System.getProperty(key, def)
				: (getParameter(key) != null ? getParameter(key) : def);
	}

	// Construct the applet
	public pdfApplet() {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());

			UIManager
					.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (UnsupportedLookAndFeelException e) {
			e.printStackTrace();
		}
	}

	// Initialize the applet
	public void init() {
		
		try {
			if(!debug) {
				window = JSObject.getWindow(this);

				window.eval("debug()");
				mode = (String) window.eval("mode");
				host = (String) window.eval("host");
			}
			
			jbInit();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public class Filter extends FileFilter         //Filter 继承 FileFilter
	 {
	  public boolean accept(File file)
	  {
	   return(file.getName().endsWith(".pdf") || file.isDirectory());
	   /* 返回要显示的文件类型 */
	  }
	  public String getDescription()
	  {
	   return("Pdf Files(*.pdf)");                  //返回显示文件类型的描述
	  }
	 }
	
	// Component initialization
	private void jbInit() throws Exception {
		this.setSize(new Dimension(1, 1));
		this.setLayout(new BorderLayout());
		final Panel panel = new Panel();	
		try {
			JFileChooser chooser = new JFileChooser();
			chooser.setDialogType(JFileChooser.OPEN_DIALOG);
			chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
			chooser.addChoosableFileFilter(new Filter());
//			chooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
			chooser.setDialogTitle("请选择PDF");
//			if(mode.equals("multi")) chooser.setMultiSelectionEnabled(true);
			int result = chooser.showOpenDialog(panel);
			if (result == 0) {
				long startTime = new Timestamp().getDateTime();
				System.out.println("开始" + startTime);

				File file = chooser.getSelectedFile();
				String name = file.getAbsolutePath();
				connect();
				if (file.isDirectory()) {
					if(!debug) window.eval("isNotFile()");
				} else if (file.isFile()) {
					if(file.getName().toLowerCase().endsWith("pdf")){
						window.eval("start()");
						sendSingleFile(file.toString(), "/"+ file.getName(), file.getName());
					} else {
						if(!debug) window.eval("isNotPdfFile()");
					}
				}
				
				String info = getInfo();
				window.eval("debug('"+info.length()+"');");
				sendEnd();

				// 计算时间结束
				long endTime = new Timestamp().getDateTime();
				System.out.println("END" + endTime);
				long timeUsed = (long) (endTime - startTime + 1) / 1000;
				long speed = (long) fileCounter / (timeUsed + 1);

				window.eval("finish('"+info+"');");
			} else {
				window.eval("cancel();");
			}
		} catch (Exception e1) {
			window.eval("error("+e1.getLocalizedMessage()+");");
		}
	}
	

	public static void main(String[] args) {
		String file = "d:\\软件工具\\FB3_WWEJ.exe";
		pdfApplet app = new pdfApplet();
		app.loadFile(file);
	}

	public void loadFile(String fileName) {
		try {
			connect();
			doSend(fileName);
			try {
				client.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (SecurityException se) {
			window.eval("securityException()");
		}
	}

	public void connect() {
		try {
			client = new Socket(host, port);
			outputStream = new DataOutputStream(client.getOutputStream());
			inputStream = new DataInputStream(client.getInputStream());

		} catch (UnknownHostException e) {
			System.out.println("未知主机 " + host + ":" + port);
		} catch (IOException e) {
			System.out.println("IO错误: " + e);
		} finally {
			// try {
			// client.close();
			// System.out.println("套接字关闭");
			// } catch (IOException e) {
			// e.printStackTrace();
			// }

		}
	}

	public void doSend(String fileName) {
		try {

			sendFile(fileName, outputStream, inputStream);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	// Get Applet information
	public String getAppletInfo() {
		return "This is an applet can read and write the local file system";
	}

	public void loadDir(File dir) {

		String[] al = matchResponse.split(",");
		for (int i = 0; i < fileListArray.size(); i++) {
			String fileName = (String) fileListArray.get(i);
			String serverFilename = "/"+al[i];
			String relativeFile = replaceStr(fileName, dir.getAbsolutePath(),
					"");
			sendSingleFile(fileName, dir.getName() + relativeFile, serverFilename);
		}

	}

	public String replaceStr(String str, String problemStr, String replace) {
		str = str.replaceAll("\\\\", "/");
		problemStr = problemStr.replaceAll("\\\\", "/");
		return str.replaceFirst(problemStr, "");
	}

	public void listFiles(String strPath) {
		File dir = new File(strPath);
		File[] files = dir.listFiles();

		if (files == null)
			return;
		for (int i = 0; i < files.length; i++) {
			String fileName = files[i].getName();
			if(fileName.equals(".version") || fileName.equals("Thumbs.db") || fileName.endsWith(".indd")) continue;
			if (files[i].isDirectory()) {
				listFiles(files[i].getAbsolutePath());
			} else {
				fileListArray.add(files[i].getAbsolutePath());
			}
		}
	}

	public void sendSingleFile(String fileName, String relativeFile, String serverFilename) {
		try {
			System.out.println("文件名:"+relativeFile);
			if(new File(fileName).length() != 0) { 
				outputStream.writeUTF(serverFilename + "|"
						+ new File(fileName).length());// 发送文件名
				outputStream.flush();
	
				sendFile(fileName, outputStream, inputStream); // 发送字节流
				System.out.println("已经结束");
	
				try {
					inputStream.readUTF();
					// if("OK".equals(inputStream.readUTF()))
					window.eval("fileFinish('"+relativeFile+"')");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public String getInfo()
	{
		String info = "";
		try {
			outputStream.writeUTF("GET_INFO");
			outputStream.flush();
			
			info = inputStream.readUTF();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return info;
	}

	public void sendEnd() {
		try {
			outputStream.writeUTF("END");

			outputStream.flush();
			client.close();
		} catch (IOException e) {
			e.printStackTrace();
		}// 发送结束
	}

	public void sendFile(String fileName, DataOutputStream outputStream,
			DataInputStream inputStream) throws IOException {
		/* 缓冲区 */
		byte[] buf = new byte[65536];
		FileInputStream fis = null;

		try {
			fis = new FileInputStream(fileName);

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		long counter = 0;
		long fileSize = new File(fileName).length();
		/* 发送数据 */
		while (counter < fileSize) {
			int read = 0;
			if (fis != null) {
				read = fis.read(buf);
			}
			if (read == -1) {
				break;
			}

			outputStream.write(buf, 0, read);
			outputStream.flush();
			counter += read;
			fileCounter += read;

		}
		System.out.println("OVER");
		outputStream.flush();
		fis.close();
	}
	
	public boolean checkIsValid(File dir)
	{
		listFiles(dir.getAbsolutePath());
		String[] validFiles = {"export.xml", "Links"};
		File[] files = dir.listFiles();
		int counter = 0;
		for(int i=0; i<files.length; i++) {
			if(files[i].isFile() && files[i].getName().equals(validFiles[0])) {
				counter++;
			}
			if(files[i].isDirectory() && files[i].getName().equals(validFiles[1])) {
				counter++;
			}
		}
		return counter >= 2;
	}
}